<?php
namespace Blogger\BlogBundle\Controller;

use Blogger\BlogBundle\Entity\Enquiry;
use Blogger\BlogBundle\Form\EnquiryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class PageController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()
                    ->getManager();

        $blogs = $em->getRepository('BloggerBlogBundle:Blog')
                    ->getLastestBlogs();

        return $this->render('BloggerBlogBundle:Page:index.html.twig', array(
            'blogs' => $blogs
        ));
    }

    public function aboutAction()
{
    return $this->render('BloggerBlogBundle:Page:about.html.twig');
}

    public function contactAction()
    {
        $enquiry = new Enquiry();
        $form = $this->createForm(new EnquiryType(), $enquiry);

        $request = $this->getRequest();
        if($request->getMethod() == 'POST') {
            $form->bind($request);

            if($form->isValid()) {

                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact enquiry from symblog')
                    ->setFrom('roger00099@gmail.com')
                    ->setTo($this->container->getParameter('blogger_blog.emails.contact_email'))
                    ->setBody($this->renderView('BloggerBlogBundle:Page:contactEmail.txt.twig', array('enquiry' => $enquiry)));
                $this->get('mailer')->send($message);

                $this->get('session')->getFlashBag()->add('blogger-notice', 'Twoja wiadomość została pomyślnie wysłana. Dziękuje!');

                return $this->redirect($this->generateUrl('BloggerBlogBundle_contact'));
            }
        }


        return $this->render('BloggerBlogBundle:Page:contact.html.twig', array('form' => $form->createView()
        ));
    }
}